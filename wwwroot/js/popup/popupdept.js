$(document).ready(function () {
    $('button[name=btnPopup_searchDept]').click(function (e) { 
        e.preventDefault();
        Dept.loadDtt(1);
    });       
    $(`table[name=dttPopup_Dept]`).on(`click`,`.btnselectClick`, () => {
      //  console.log(this);
      // console.log(this.activeElement.parentNode.parentNode);
        var btnCurrent = this.activeElement.parentNode.parentNode;
        Dept.select(btnCurrent);
       $('#popupDepartment').modal('hide');
    }); 
});

/*----------------------------function----------------------------------*/
let   dttDept;
let Dept = {
    callback:null,
    show: function(p_callback){
        this.callback = p_callback;
        $('#frmPopup_Dept input').val('');
        $('#popupDepartment').modal('show');
        this.loadDtt();
    },
    select: function(btn){
       // const  values = dttPos.rows('.selected').data().toArray();
        const values = dttDept.row(btn).data();
        let dataObj = {
                deptid : values.deptCd,
                deptname: values.deptName
            }
         this.callback(dataObj);
    },
    loadDtt: function(key){
        switch (key) {
            case 1:
                if($.fn.DataTable.isDataTable(`table[name=dttPopup_Dept]`)){
                    dttDept.destroy();
                    dttDept = $(`table[name=dttPopup_Dept]`).DataTable({
                        searching: false,
                        lengthChange:false,
                        ajax: {
                                global:false,
                                url: `PopupDept/loadData`,
                                type: `POST`,
                                dataType: `json`,
                                data: (d) => {
                                        return $.extend({}, d, {
                                            "deptid": $(`#txtpopup_Deptid`).val(),
                                            "deptname": $(`#txtpopup_Deptname`).val(),                                                
                                        });
                                    },
                                dataSrc:(json)=> {   
                                    if (json.data == null) {
                                        alert(`ไม่พบข้อมูล`);
                                        return false;
                                    } else {
                                        return json.data;
                                    }
                                },
                                error:  (xhr, error, thrown) => {
                
                                }
                                },
                                processing: true,
                                deferLoading: 0,
                                serverSide: true,
                                pageLength : 5,
                                columns: [  
                                    {
                                        data: `deptCd`
                                    },  
                                    {
                                        data: `deptName`, 
                                    },  
                                    {
                                        data: `deptCd`
                                    },
                                ],
                                columnDefs: [
                                    {
                                        targets: 0, 
                                        className: `text-center`,
                                        width: "20%",
                                    },
                                    {
                                        targets: 2,
                                        render:  (data) =>`<button class="btn btn-outline-primary btn-sm btnselectClick"  name="positionId" type="button" value="${data}"><i class="fas fa-check"></i></button>`                              
                                    },
                                    {
                                        targets: 2, 
                                        className: `text-center`,
                                        width: "15%",
                                        orderable: false
                                    },
                                ],
                        });
                    }
                    dttDept.ajax.reload();
                break;
        
            default:
                if(!($.fn.DataTable.isDataTable('table[name=dttPopup_Dept]'))){
                    dttDept = $('table[name=dttPopup_Dept]').DataTable({
                        searching:false,
                        lengthChange:false,
                        ordering: false
                    });
                }else{
                    dttDept.clear();
                    dttDept.destroy();
                    dttDept = $('table[name=dttPopup_Dept]').DataTable({
                        searching:false,
                        lengthChange:false,
                        ordering: false
                    });
                }
                break;
        }
    }
}
