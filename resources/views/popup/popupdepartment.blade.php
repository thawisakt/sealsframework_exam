<!-- Modal -->
<div class="modal fade" id="popupDepartment" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <form action="" method="POST" name="frmPopup_Dept" id="frmPopup_Dept">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ค้นหาข้อมูลหน่วยงาน</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="w-100">
                <div class="form-group row">
                    <div class="col-sm-2 text-right">
                        <label for="lbDeptId">รหัสหน่วยงาน</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="text" name="txtpopup_Deptid" id="txtpopup_Deptid"  class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-2 text-right">
                        <label for="lbDeptName">ชื่อหน่วยงาน</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="txtpopup_Deptname" id="txtpopup_Deptname" class="form-control">
                    </div>
                    <div class="col-sm-2">
                        <button type="button" name="btnPopup_searchDept"  class="btn btn-info"><i class="fas fa-search"></i>ค้นหา</button>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="table responsive table-bordered">
                        <table class="table" name="dttPopup_Dept">
                            <thead>
                                <tr>
                                    <th>รหัสหน่วยงาน</th>
                                    <th>ชื่อหน่วยงาน</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
        </div>       
    </div>
    </form>
</div>