@extends('layout')

@section('content')
@include('popup.popupdepartment')
    <div class="container">
        <div class="card border-warning" >
            <div class="card-header  bg-warning">
                <h4>หน้าจอประชาสัมพันธ์ข่าว</h4>
            </div>
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="newsInfo-tab" data-toggle="tab" href="#newsInfo" role="tab" aria-controls="newsInfo" aria-selected="true">ข้อมูลข่าวประชาสัมพันธ์</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="newsAdd-tab" data-toggle="tab" href="#newsAdd" role="tab" aria-controls="newsAdd" aria-selected="false">บันทึกข่าวประชาสัมพันธ์</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="newsInfo" role="tabpanel" aria-labelledby="newsInfo-tab">
                        <div class="card">
                            {{-- <div class="card-header">
                                <button type="button" class="btn btn-success" id="btnAddnews">เพิ่ม</button>
                                <button type="button" class="btn btn-success">Export Excel</button>
                                <button type="button" class="btn btn-success">Export CVS</button>
                                <button type="button" class="btn btn-danger">Export PDF</button>
                            </div> --}}
                            <div class="card-body">
                                <table class="table table-bordered table-reponsive" id="dttNews">
                                    <thead class="table-primary">
                                        <tr>
                                            <th>รหัสข่าว</th>
                                            <th>หน่วยงานที่แจ้งข่าว</th>
                                            <th>หัวข้อข่าว</th>
                                            <th>วันที่เริ่ม - สิ้นสุด</th>
                                            <th>แก้ไข</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="newsAdd" role="tabpanel" aria-labelledby="newsAdd-tab">
                        <div class="card">
                            {{-- <div class="card-header"></div> --}}
                            <form method="POST" id="frmnewsAdd_Tab" action="" >
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbNewsid">รหัสข่าว</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" name="txtNewsId" id="txtNewsId" class="form-control" disabled>
                                        <input type="hidden" name="hidtxtNewsId" id="hidtxtNewsId" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbBudyear">ปีงบประมาณ</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" name="txtBudyear" id="txtBudyear" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbNewstype">ประเภทข่าว</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <select name="slNewsType" id="slNewsType" class="form-control"></select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbDept">หน่วยงาน</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="input-group">
                                            <input type="text" name="txtDeptId" id="txtDeptId" class="form-control">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-info" name="btnDept" id="btnDept" data-toggle="modal" data-target="#popupDepartment"><i class="fa fa-angle-down"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" name="txtDeptName" id="txtDeptName" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbNewsTitle">หัวข้อข่าว</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <input type="text" name="txtNewsTitle" id="txtNewsTitle" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbDate">ช่วงวันที่</label>
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" name="txtStartDate" id="txtStartDate" class="form-control" placeholder="__/__/____">
                                    </div>
                                    <div class="col-sm-5">
                                        <input type="text" name="txtEndDate" id="txtEndDate" class="form-control" placeholder="__/__/____">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbNewsDetail">รายละเอียดข่าว</label>
                                    </div>
                                    <div class="col-sm-10">
                                        <textarea name="summernote" id="summernote" ></textarea>
                                    </div>
                                </div>
                            </form>
                                <div class="form-group row">
                                    <div class="col-sm-2 text-right">
                                        <label for="lbNewsFile">เอกสารประกอบ</label>
                                    </div>
                                    <div class="col-sm-2">
                                        <button type="button" name="btnAddfile" id="btnAddfile" class="btn btn-success" data-toggle="modal" data-target="#modelfile"><i class="fas fa-file-alt"></i> เพิ่มเอกสาร</button>
                                        {{-- <button type="button" name="btntestdropzone" id="btntestdropzone" class="btn btn-danger">TestDrop</button> --}}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <form action="" id="frmfile">
                                        <table class="table table-bordered" id="dttFile">
                                            <thead class="table-primary">
                                                <tr>
                                                    <th>ชื่อไฟล์</th>
                                                    <th>ไฟล์</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody id="dttFile_body" >
                                            </tbody>
                                        </table>
                                    </form>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="offset-sm-9 col-sm-3 text-right">
                                        <button type="submit" name="btnSave" id="btnSave" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
                                        <button type="button" name="btnCancle" id="btnCancle" class="btn btn-secondary"><i class="far fa-window-close"></i> ยกเลิก</button>
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="dropzone" id="myDropzon"></div> --}}
                   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal POPUP UPLOAD IMAGE -->
    <div class="modal fade" id="modelfile" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">เลือกไฟล์</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                    </div>
                    <div class="modal-body">
                        <div class="dropzone" id="dropzone_file"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="btnUpload_image">Upload</button>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('script')
<script src="@relative('js/popup/popupdept.js')"></script>
<script>
    let dropZone;
    Dropzone.autoDiscover = false;
    $(document).ready(function () {
        

        $('#slNewsType').select2({
            placeholder: "กรุณาเลือกประเภทข่าว",
            width: "100%",
            ajax:{
                url: "{{route('News/loadNewstype')}}",
                dataType: "json",
                processResults: function(data,params){
                    return {
                        results : $.map(data,function(item,index){
                            return {
                                text: item.NewsTypename,
                                id: item.NewTypeid
                            }
                        })
                    };
                }
            }
        });
        //----Popup-----//
        $('#btnDept').click(function (e) { 
                e.preventDefault();
                Dept.show((obj)=>{
                    $('#txtDeptId').val(obj.deptid);
                    $('#txtDeptName').val(obj.deptname);
                });
            });
        
        $('#txtStartDate').flatpickr({
            //allowInput: true,
            dateFormat: 'd/m/Y'
        });
        let startDate;
        $('#txtEndDate').flatpickr({
            //allowInput: true,
            dateFormat: 'd/m/Y',
        });


        $('#summernote').summernote({
            height: 100
        });

        let dttFile = $('#dttFile').DataTable({
            searching:false,
            lengthChange:false,
            pageLength: 5,
        });

//         $(`div#dropzone_file`).dropzone({ 
//             url: `News/upload`,
//             method: `POST`,
//             addRemoveLinks: true,
//             autoProcessQueue: false,
//             uploadMultiple:true,
//             parallelUploads: 50,
//             init: function () {
//                 dropZone = this;
//                 $('#btnUpload_image').click(function (e) { 
//                     e.preventDefault();
//                     //  dropZone.processQueue(); 
//                     $('#modelfile').modal('hide');
//                     //filePreview(dropZone);
//                     //console.log(dropZone);
//                     countFiles= dropZone.files.length;
//                     if($.fn.dataTable.isDataTable('#dttFile')){
//                         dttFile.clear();
//                         dttFile.destroy();
//                         dttFile = $('#dttFile').DataTable({
//                             searching:false,
//                             lengthChange:false,
//                             pageLength: 5,
//                         });
//                         for (let index = 0; index < countFiles; index++) {
//                             filenameOrigin = dropZone.files[index].name;
//                             dttFile.row.add([
//                                 `<input type="text" name="txtfilename[]" id="txtfilename${index}" class="form-control" ></input>`,
//                                 `<a class="filedropzone" ><i class="fas fa-file-alt"></i> ${filenameOrigin}</a>`
//                             ]).draw(false).node(); 
                           
//                         }   
//                     }  
//                 });
//                 this.on('sending', function(file, xhr, formData){
//                     //console.log($('#frmfile').serialize());
//                     //formData.append('param',$('#frmfile').serialize());  
//                     for (let index = 0; index < dropZone.files.length; index++) {
//                         //const element = array[index];
//                         formData.append('param['+index+']', $('#txtfilename'+index).val());
//                     }        
//                 });
//             },
//             success: function (file, response) { 
//                 //console.log(file);
//             },
//             error: function (file, response)  {
//                 	alert(response);
//                 this.removeFile(file);
//           	}
//         });  

        // $('#modelfile').on('show.bs.modal', function(e){
        //     console.log(dropZone);
        //     if(dropZone.files.length>0){
        //         for (let index = 0; index < dropZone.files.length; index++) {
        //             //const element = array[index];
        //             console.log(dropZone.files[index].status);
        //             //let ss;
        //             if(dropZone.files[index].status == "success"){
        //                 dropZone.removeFile(ss);  
        //             }
        //            // dropZone.removeFile(ss);
        //         }
        //     }
        // });

       
        $('#btnCancle').click(function (e) { 
            e.preventDefault();
            $('.nav-item a[href="#newsInfo"]').tab('show');
        });


        $('#btnSave').click(function (e) { 
             e.preventDefault();
            let frm = $('#frmnewsAdd_Tab');
            //dropZone.processQueue();
            $.ajax({
                type: "post",
                url: "{{route('News/save')}}",
                data: frm.serialize(),
                dataType: "json",
                success: function (response) {
                   // console.log(response.ms_alert);
                    $.each(response, function (indexInArray, valueOfElement) { 
                         alert(valueOfElement);
                    });
                }
            });
            //myDropzone.processQueue();
            //dropZone.processQueue();
        });

        /*-----------------------testDropzone class---------------------------*/
        /*
        var previewNode = document.querySelector( '#template' );
        previewNode.id = '';
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild( previewNode );


        var myDropzone = new Dropzone( document.body, {
            url: 'News/upload',
            previewTemplate: previewTemplate,
            autoProcessQueue: false,
            previewsContainer: '#previews',
            clickable: '#btnAddfile',
            addRemoveLinks: false,
            uploadMultiple:true,
            parallelUploads: 50,
            init: function () {
               // dropZone = this;
                this.on('sending', function(file, xhr, formData){
               // console.log(formData);
               // countFiles= myDropzone.files.length;
               // console.log(countFiles);
                // for (let index = 0; index < countFiles; index++) {
                //    // $fileName = dropZone.files[index].name;    
                //     customName = $("input[name=txtfilename").val();
                //     //console.log(customName);
                //     formData.append('param['+index+']', customName);     
                // }   
                formData.append('param',$('#frmfile').serialize());     
                      
                });
            },
           
        } );
        let filenameOrigin;
        myDropzone.on( 'addedfile', function( file ) {
            //file.previewElement.querySelector( '.start' ).onclick = function() { myDropzone.enqueueFile( file ); };
            console.log(myDropzone);
            //console.log(previewTemplate);
            countFiles= myDropzone.files.length;
            for (let index = 0; index < countFiles; index++) {
                filenameOrigin = myDropzone.files[index].name;
            }
            dttFile.row.add([
                    `<input type="text" name="txtfilename[]" class="form-control" ></input>`,
                    `<a class="filedropzone" ><i class="fas fa-file-alt"></i> ${filenameOrigin}</a>`
                ]).draw(false).node();  
                //file.previewElement.querySelector("#btnSave").onclick = function() { myDropzone.enqueueFile(file); };
                $("#btnSave").click(function (e) { 
                    e.preventDefault();
                    //console.log(file);
                    myDropzone.processQueue();
                });
        } );
        */



        /*-------------------script tab1-info---------------*/

        let dttNews = $('#dttNews').DataTable({
            ajax:{
                url:"{{route('News/listData')}}",
                type:'POST',
                datatype:'json',
                dataSrc:(json)=>{
                    if(json.data == null){
                        alert ("ไม่พบข้อมูล");
                        return false;
                    }else{
                        return json.data;
                    }
                },
                error:(xhr, error, thrown) =>{

                }
            },
            processing: true,
            serverSide: true,
            responsive: true,
            searching:false,
            pageLength: 5,
            columns: [
                {
                    data:'newsId',
                    name:'newsId'
            },
                {
                    data:'deptName',
                    name:'deptName'
            },
                {
                    data:'newsTitle',
                    name:'newsTitle'
            },
                {
                    data:'startDate',
                    name:'startDate',
                    render:(data,type,row)=>{
                        let formate_startDate = moment(data.date);
                        let formate_endDate = moment(row['endDate'].date);
                        let startDate = formate_startDate.format('DD/MM/YYYY');
                        let endDate = formate_endDate.format('DD/MM/YYYY');
                        return `${startDate} - ${endDate}`;
                    }
            },
                {
                    data:'newsId',
                    orderable:false,
                    render:(data)=>`<button type="button" class="btn btn-secondary" id="btnEdit" data-id="${data}"><i class="fas fa-edit"></i> แก้ไข</button>`
            }
            ],
            columnDefs:[
                {
                    targets:0,
                    className:'text-center'
                },
                {
                    targets:3,
                    className:'text-center'
                },
                {
                    targets:4,
                    className: 'text-center'
                }
            ],
            dom:"Btip",
            buttons: [{
                text: 'เพิ่ม',
                className: 'btn btn-success btnAddnews',
            },
            {
                extend: 'excel',
                title: 'ข้อมูลข่าวประชาสัมพันธ์',
                filename: 'News_Information',
                exportOptions:{
                    columns:[0,1,2,3]
                },
                text: '<i class="fas fa-file-excel"></i> Export Excel',
                className: 'btn btn-success'
            },
            {
                extend: 'csv',
                charset:'UTF-8',
                bom: true,
                title: 'ข้อมูลข่าวประชาสัมพันธ์',
                filename: 'News_Information',
                exportOptions:{
                    columns:[0,1,2,3]
                },
                text: '<i class="fas fa-file-csv"></i> Export CSV',
                className: 'btn btn-success'
            },
            {
                extend: 'pdf',
                title: 'ข้อมูลข่าวประชาสัมพันธ์',
                filename: 'News_Information',
                exportOptions:{
                    columns:[0,1,2,3]
                },
                text: '<i class="fas fa-file-pdf"></i> Export PDF',
                className: 'btn btn-danger'
            }]
        });

        $('.btnAddnews').click(function (e) { 
            e.preventDefault();
            $('.nav-item a[href="#newsAdd"]').tab('show');
            clearTabadd();
        });
        

        $('#dttNews tbody').on('click','#btnEdit', function (e) {
            let news_id = e.currentTarget.dataset.id;
            $('.nav-item a[href="#newsAdd"]').tab('show');
            getData(news_id);

        });
        $('#newsInfo-tab').click(function (e) { 
            e.preventDefault();
            dttNews.ajax.reload();
            
        });
        $('#newsAdd-tab').click(function (e) { 
            e.preventDefault();
            clearTabadd();
        });

        /*--------------function get data to edit----------*/
        function getData(v_newsid){
            $.ajax({
                type: "post",
                url: `{{route('News/getdata')}}/${v_newsid}`,
                data: "data",
                dataType: "json",
                success: function (response) {
                   $.each(response, function (indexInArray, valueOfElement) { 
                   // console.log(valueOfElement);
                    $('#txtNewsId').val(valueOfElement.newsid);
                    $('#hidtxtNewsId').val(valueOfElement.newsid);
                    $('#txtBudyear').val(valueOfElement.budyear);
                    if(valueOfElement.newstypecd != null){
                        $('#slNewsType').append(`<option value="${valueOfElement.newstypecd}">${valueOfElement.newstypename}</option>`);
                    }else{
                        $('#slNewsType').val(``);
                    }
                    $('#txtDeptId').val(valueOfElement.deptcd);
                    $('#txtDeptName').val(valueOfElement.deptname);
                    $('#txtNewsTitle').val(valueOfElement.newstitle);
                    let sdate = moment(valueOfElement.startdate.date);
                    let s_fdate = sdate.format('DD/MM/YYYY');
                    $('#txtStartDate').val(s_fdate);
                    let edate = moment(valueOfElement.enddate.date);
                    let e_fdate = edate.format('DD/MM/YYYY');
                    $('#txtEndDate').val(e_fdate);
                    $('#summernote').summernote('code',valueOfElement.newsdetail);
                   });
                }
            });
        }

        /*---------------function clear tab add---------------   */
        function clearTabadd(){
            $('#frmnewsAdd_Tab input').val('');
            $('#slNewsType').empty();
            $('#summernote').summernote('code','')
        }

    });
</script>
    
@endsection