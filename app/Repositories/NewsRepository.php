<?php
namespace App\Repositories;
use App\Entities\News;
use App\Entities\Department;
use App\Entities\FileMaster;
use App\Entities\NewsFile;
use Doctrine\ORM\Tools\Pagination\Paginator; 

class NewsRepository extends BaseRepository implements BaseInterface{
    private $em;
    public function __construct(){
        parent::__construct();
        $this->em = $this->getEntityManager();
    }
    public function listByPage($start,$length,$orderColName,$strOrderDirection){
        //var_dump($strOrderDirection);
        $obPrefix;
        if($orderColName=="deptCd"){
            $obPrefix = "d";
        }else{
            $obPrefix = "n";
        }
        $query = $this->em->createQueryBuilder()
        ->select('n.newsId',
                'd.deptName',
                'n.newsTitle',
                'n.startDate',
                'n.endDate',
                'n'
                )
        ->from(News::class,'n')
        ->leftJoin('n.deptCd','d')
        ->orderBy($obPrefix.'.'.$orderColName,$strOrderDirection)
        ->getQuery();
        $query->setFirstResult($start)->setMaxResults($length); 
        //var_dump($query->getDql());
    //      echo $query->getDql();
    // echo $query->getSQL();
        $paginator = new Paginator($query);
        return $paginator;
    }
    public function list(){}
    public function get($id){
        return $this->em->getRepository(News::class)->findOneBy(['newsId'=>$id]); 
    }
    
    public function getdata($id){
        $query = $this->em->createQueryBuilder()
       ->select('n.newsId',
                'nt.newsTypeCd',
                'nt.newsTypeName',
                'd.deptCd',
                'd.deptName',
                'n.budYear',
                'n.startDate',
                'n.endDate',
                'n.newsTitle',
                'n.newsDetail')
       ->from(News::class,'n')
       ->leftJoin('n.newsTypeCd','nt')
       ->leftJoin('n.deptCd','d')
       ->where('n.newsId=:id')
       ->setParameter('id',$id)
       ->getQuery();
      // echo $query->getDql();
       return $query->getOneOrNullResult();
    }
    public function save($data){
        $news = $this->get($data->getNewsId());
       // \var_dump($news);
        $newsid = $data->getNewsId();
        $newstype = $data->getNewsTypeCd();
        $dept = $data->getDeptCd();
        $budyear = $data->getBudYear();
        $startdate = $data->getStartDate();
        $enddate = $data->getEndDate();
        $newstitle = $data->getNewsTitle();
        $newsdetail = $data->getNewsDetail();
        if($news!=null){
            $this->em->getConnection()->beginTransaction();
            try {
              //  echo "1";
                //\var_dump($budyear);
                $query =$this->em->createQueryBuilder()
                ->update(News::Class,'n')
                ->set('n.budYear',':budyear')
                ->set('n.startDate',':startdate')
                ->set('n.endDate',':enddate')
                ->set('n.newsTitle',':newtitle')
                ->set('n.newsDetail',':newdetail')
                ->set('n.deptCd',':deptcd')
                ->set('n.newsTypeCd',':newstypecd')
                ->where('n.newsId=:newid')
                ->setParameter('newid',$newsid)
                ->setParameter('budyear',$budyear)
                ->setParameter('startdate',$startdate)
                ->setParameter('enddate',$enddate)
                ->setParameter('newtitle',$newstitle)
                ->setParameter('newdetail',$newsdetail)
                ->setParameter('deptcd',$dept)
                ->setParameter('newstypecd',$newstype)
                ->getQuery();
               // echo "2";
                //echo $query->getDql();
                $query->execute();
               // echo "3";
                $this->em->getConnection()->commit();

            } catch (\Exception $e) {
              //  echo "4";
                $this->em->getConnection()->rollBack();
                throw $e;
            } 
        }else{
            $this->em->getConnection()->beginTransaction();
            try {
                $this->em->merge($data);
                $this->em->flush();
                $this->em->getConnection()->commit();
            } catch (\Exception $e) {
                $this->em->getConnection()->rollBack();
                throw $e;
            }
        }
    }
    public function delete($id){}
    public function save2($data,$file,$newsfile){
        $news = $this->get($data->getNewsId());
        $newsid = $data->getNewsId();
        $newstype = $data->getNewsTypeCd();
        $dept = $data->getDeptCd();
        $budyear = $data->getBudYear();
        $startdate = $data->getStartDate();
        $enddate = $data->getEndDate();
        $newstitle = $data->getNewsTitle();
        $newsdetail = $data->getNewsDetail();
        if($news!=null){
            $this->em->getConnection()->beginTransaction();
            try {
                $query =$this->em->createQueryBuilder()
                ->update(News::Class,'n')
                ->set('n.newId',':newid')
                ->set('n.budYear',':budyear')
                ->set('n.startDate',':startdate')
                ->set('n.endDate',':enddate')
                ->set('n.newsTitle',':newtitle')
                ->set('n.newsDetail',':newdetail')
                ->set('n.deptCd',':deptcd')
                ->set('n.newsTypeCd',':newstypecd')
                ->where('n.newId=:newid')
                ->setParameter('newid',$news)
                ->setParameter('budyear',$startdate)
                ->setParameter('startdate',$enddate)
                ->setParameter('enddate',$enddate)
                ->setParameter('newtitle',$newstitle)
                ->setParameter('newdetail',$newsdetail)
                ->setParameter('deptcd',$dept)
                ->setParameter('newstypecd',$newstype)
                ->getQuery();
                $query->execute();

                $this->em->getCOnnection()->commit();

            } catch (\Exception $e) {
                $this->em->getConnection()->rollBack();
                throw $e;
            } 
        }else{
            $this->em->getConnection()->beginTransaction();
            try {
                $this->em->merge($data);
                $this->em->flush();

                $entityName = get_class($data);
                $query =  $this->em->createQueryBuilder()
                ->select('coalesce (max(n.newsId),1) as pk')->from($entityName,'n')
                ->getQuery();
                $query = $query->getOneOrNullResult();
                $newsid = $query['pk'];
                $re_newsid = $this->get($newsid);

                for ($i=0;$i < count($file); $i++){ 
                    // $this->em->merge($file[$i]);
                    // $this->em->flush();
                    $entityNamefile = get_class($file[$i]);
                    $query =  $this->em->createQueryBuilder()
                    ->select('coalesce (max(f.fileId)+1,1) as pk')->from($entityNamefile,'f')
                    ->getQuery();
                    $query = $query->getOneOrNullResult();
                    $fileid = $query['pk'];
                    $filename = $file[$i]->getFileName();
                    $filepath = $file[$i]-getFilePath();
                    $full_filepath = $filepath.'/'.$fileid;
                    $filemimetype= $file[$i]->getFileMimeType();
                    $file[$i]->setFileName($filename);
                    $file[$i]->setFilePath($filepath);
                    $file[$i]->setFileMimeType($filemimetype);

                    $newsfile[$i]->setRunning($re_newsid,2);
                    $newsfile[$i]->setNewsId($re_newsid);
                    $newsfile[$i]->setFileId($fileid);
                }
                $this->em->getConnection()->commit();
            } catch (\Exception $e) {
                $this->em->gteConnection()->rollBack();
                throw $e;
            }
        }
    }
}
