<?php

namespace App\Repositories;
use App\Entities\NewsType;

class NewstypeRepository extends BaseRepository implements BaseInterface{
    private $em;
    public function __construct(){
        parent::__construct();
        $this->em = $this->getEntityManager();
    }
    public function list(){
       $query = $this->em->createQueryBuilder()
        ->select('nt.newsTypeCd','nt.newsTypeName','nt.newsTypeKey')
        ->from(NewsType::class,'nt')
        ->orderBy('nt.newsTypeCd', 'ASC')
        ->getQuery();
        return $query->getResult();
    }
    public function get($id){
        $query = $this->em->createQueryBuilder()
        ->select('nt.newsTypeCd','nt.newsTypeName','nt.newsTypeKey')
        ->from(NewsType::class,'nt')
        ->where('nt.newsTypeCd=:id')
        ->setParameter('id',$id)
        ->getQuery();
        return $query->getOneOrNullResult();
    }
    public function save($data){

    }
    public function delete($id){
        
    }

    public function getid($id){
        return $this->em->getRepository(NewsType::class)->findOneBy(['newsTypeCd'=>$id]); 
    }
}

