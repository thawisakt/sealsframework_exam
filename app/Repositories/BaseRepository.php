<?php 
namespace App\Repositories;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
class BaseRepository { 
    private $entity;
    public function __construct() {
        $isDevMode = true;
        $dbParam = array (
            'url' => getenv("DB_URL")
        ); 
        $paths = array(BASE_PATH . "/app/Entities");
        $proxiesPath = BASE_PATH. "/bootstrap/proxies"; 
       // print(BASE_PATH);

        $config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
        $this->entity = EntityManager::create($dbParam,$config); 
        $dbh = $this->entity->getConnection(); 

       //Function_Custom_create
        $config->addCustomStringFunction('CONCATHELLO','App\Classes\DQL\ConcatHello');
        $config->addCustomStringFunction('INITCAP','App\Classes\DQL\Initcap');
        $config->addCustomNumericFunction('FLOOR','App\Classes\DQL\Floor');
        $config->addCustomNumericFunction('CEIL','App\Classes\DQL\Ceil');
        $config->addCustomNumericFunction('ROUND','App\Classes\DQL\Round');
        
        //Function Date
        $config->addCustomDatetimeFunction('day', 'DoctrineExtensions\Query\Oracle\Day');
        $config->addCustomDatetimeFunction('month', 'DoctrineExtensions\Query\Oracle\Month');
        $config->addCustomDatetimeFunction('year', 'DoctrineExtensions\Query\Oracle\Year');
        $config->addCustomDatetimeFunction('to_char', 'DoctrineExtensions\Query\Oracle\ToChar');
        $config->addCustomDatetimeFunction('trunc', 'DoctrineExtensions\Query\Oracle\Trunc');

        //Function String
        $config->addCustomStringFunction('nvl','DoctrineExtensions\Query\Oracle\Nvl');
        $config->addCustomStringFunction('listagg','DoctrineExtensions\Query\Oracle\Listagg');
        $config->addCustomStringFunction('to_date','DoctrineExtensions\Query\Oracle\ToDate');

        //set Proxy
        $config->setProxyDir($proxiesPath);
        $config->setProxyNamespace('GeneratedProxies');

        if($isDevMode){
            $config->setAutoGenerateProxyClasses(true);
        }else{
            $config->setAutoGenerateProxyClasses(false);
        }

        $sth = $dbh->prepare("ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD HH24:MI:SS'");
        $sth->execute();
    }
    protected function getEntityManager() {
        return $this->entity;
    }
}