<?php

namespace App\Repositories;
use App\Entities\Department;
use Doctrine\ORM\Tools\Pagination\Paginator; 

class DepartmentRepository extends BaseRepository implements BaseInterface{
    private $em;
    public function __construct(){
        parent::__construct();
        $this->em = $this->getEntityManager();
    }
    public function listByNamePage($deptid,$deptname,$start,$length,$orderColName,$strOrderDirection){
        $query = $this->em->createQueryBuilder()
        ->select('d.deptCd',
                'd.deptName',
                'd')
        ->from(Department::class,'d')
        ->where('d.deptCd LIKE :deptid')
        ->andWhere('d.deptName LIKE :deptname')
        ->setparameter('deptid','%'.$deptid.'%')
        ->setParameter('deptname','%'.$deptname.'%')
        ->orderBy('d.'.$orderColName,$strOrderDirection)
        ->getQuery();
        $query->setFirstResult($start)->setMaxResults($length); 
        $paginator = new Paginator($query);
        return $paginator;
    }
    public function list(){
        $query = $this->em->createQueryBuilder()
        ->select('d.deptCd',
                'd.deptName',
                'd')
        ->from(Department::class,'d')
        ->getQuery();
        return $query->getResult();
    }

    public function get($id){
        $query = $this->em->createQueryBuilder()
        ->select('d.deptCd','d.deptName')
        ->from(Department::class,'d')
        ->where('d.deptCd=:id')
        ->setParameter('id',$id)
        ->getQuery();
        return $query->getOneOrNullResult();
    }
    public function save($data){

    }
    public function delete($id){
        
    }
    
    public function getid($id){
        return $this->em->getRepository(Department::class)->findOneBy(['deptCd'=>$id]); 
    }
}