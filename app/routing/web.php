<?php


$router->map('GET','/','App\Controllers\TestController@index','get-get5');
$router->map('GET', '/get_json', 'App\Controllers\TestController@get_json', 'getjson');
$router->map('GET', '/[a:controller]', 'App\Controllers\{controller}Controller@index', 'default-get');
$router->map('GET', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-get2');
$router->map('GET', '/[a:controller]/[a:method]/[*:star]?', 'App\Controllers\{controller}Controller@{method}', 'default-get3');
$router->map('POST', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-POST');
$router->map('POST', '/[a:controller]/[a:method]/[a:a]?', 'App\Controllers\{controller}Controller@{method}', 'default-POST2');


$router->map('GET','/admin/user/list','App\Controllers\HomeController@List','get-list');
$router->map('GET','/admin/user/get/[i:id]','App\Controllers\HomeController@Get','get-list2');
$router->map('DELETE', '/[a:controller]/[a:method]/[i:id]?', 'App\Controllers\{controller}Controller@{method}', 'default-DELETE');