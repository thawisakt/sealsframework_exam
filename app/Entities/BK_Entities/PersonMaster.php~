<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonMaster
 *
 * @ORM\Table(name="PERSON_MASTER", indexes={@ORM\Index(name="IDX_585DA819C3C4B237", columns={"DEPT_CD"}), @ORM\Index(name="IDX_585DA81994C3C196", columns={"FILE_ID"}), @ORM\Index(name="IDX_585DA819372E891B", columns={"PERSON_TYPE_CD"}), @ORM\Index(name="IDX_585DA819922512C4", columns={"POSITION_CD"})})
 * @ORM\Entity
 */
class PersonMaster
{
    /**
     * @var string
     *
     * @ORM\Column(name="PERSON_ID", type="string", length=6, nullable=false, options={"comment"="รหัสบุคลากร"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="PERSON_MASTER_PERSON_ID_seq", allocationSize=1, initialValue=1)
     */
    private $personId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FIRST_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อ"})
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_NAME", type="string", length=100, nullable=true, options={"comment"="นามสกุล"})
     */
    private $lastName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="BIRTH_DATE", type="date", nullable=true, options={"comment"="วันเกิด"})
     */
    private $birthDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ID_CARD", type="string", length=13, nullable=true, options={"comment"="เลขที่บัตรประชาชน"})
     */
    private $idCard;

    /**
     * @var \App\Entities\Department
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DEPT_CD", referencedColumnName="DEPT_CD")
     * })
     */
    private $deptCd;

    /**
     * @var \App\Entities\FileMaster
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\FileMaster")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="FILE_ID")
     * })
     */
    private $file;

    /**
     * @var \App\Entities\PersonType
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\PersonType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSON_TYPE_CD", referencedColumnName="PERSON_TYPE_CD")
     * })
     */
    private $personTypeCd;

    /**
     * @var \App\Entities\Position
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Position")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="POSITION_CD", referencedColumnName="POSITION_CD")
     * })
     */
    private $positionCd;


}
