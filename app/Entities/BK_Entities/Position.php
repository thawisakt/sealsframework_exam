<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Position
 *
 * @ORM\Table(name="POSITION")
 * @ORM\Entity
 */
class Position
{
    /**
     * @var int
     *
     * @ORM\Column(name="POSITION_CD", type="integer", nullable=false, options={"comment"="รหัสตำแหน่ง"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="POSITION_POSITION_CD_seq", allocationSize=1, initialValue=1)
     */
    private $positionCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="POSITION_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อตำแหน่ง"})
     */
    private $positionName;



    /**
     * Get positionCd.
     *
     * @return int
     */
    public function getPositionCd()
    {
        return $this->positionCd;
    }

    /**
     * Set positionName.
     *
     * @param string|null $positionName
     *
     * @return Position
     */
    public function setPositionName($positionName = null)
    {
        $this->positionName = $positionName;

        return $this;
    }

    /**
     * Get positionName.
     *
     * @return string|null
     */
    public function getPositionName()
    {
        return $this->positionName;
    }
}
