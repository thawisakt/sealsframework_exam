<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * SysGenerate
 *
 * @ORM\Table(name="SYS_GENERATE")
 * @ORM\Entity
 */
class SysGenerate
{
    /**
     * @var string
     *
     * @ORM\Column(name="TABLE_CD", type="string", length=30, nullable=false, options={"comment"="ชื่อ Table ที่ Generate"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $tableCd;

    /**
     * @var string
     *
     * @ORM\Column(name="KEY_CD", type="string", length=100, nullable=false, options={"comment"="รหัส Key ที่ใช้ในการ Gen"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $keyCd;

    /**
     * @var int|null
     *
     * @ORM\Column(name="LAST_NO", type="integer", nullable=true, options={"comment"="เลขที่สุดท้าย"})
     */
    private $lastNo;



    /**
     * Set tableCd.
     *
     * @param string $tableCd
     *
     * @return SysGenerate
     */
    public function setTableCd($tableCd)
    {
        $this->tableCd = $tableCd;

        return $this;
    }

    /**
     * Get tableCd.
     *
     * @return string
     */
    public function getTableCd()
    {
        return $this->tableCd;
    }

    /**
     * Set keyCd.
     *
     * @param string $keyCd
     *
     * @return SysGenerate
     */
    public function setKeyCd($keyCd)
    {
        $this->keyCd = $keyCd;

        return $this;
    }

    /**
     * Get keyCd.
     *
     * @return string
     */
    public function getKeyCd()
    {
        return $this->keyCd;
    }

    /**
     * Set lastNo.
     *
     * @param int|null $lastNo
     *
     * @return SysGenerate
     */
    public function setLastNo($lastNo = null)
    {
        $this->lastNo = $lastNo;

        return $this;
    }

    /**
     * Get lastNo.
     *
     * @return int|null
     */
    public function getLastNo()
    {
        return $this->lastNo;
    }
}
