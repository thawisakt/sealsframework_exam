<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsFile
 *
 * @ORM\Table(name="NEWS_FILE", indexes={@ORM\Index(name="IDX_DC41F17094C3C196", columns={"FILE_ID"}), @ORM\Index(name="IDX_DC41F170B2ACE15A", columns={"NEWS_ID"})})
 * @ORM\Entity
 */
class NewsFile
{
    /**
     * @var string
     *
     * @ORM\Column(name="NEWS_FILE_ID", type="string", length=10, nullable=false, options={"comment"="รหัสไฟล์ข่าว"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="NEWS_FILE_NEWS_FILE_ID_seq", allocationSize=1, initialValue=1)
     */
    private $newsFileId;

    /**
     * @var \App\Entities\FileMaster
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\FileMaster")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="FILE_ID")
     * })
     */
    private $file;

    /**
     * @var \App\Entities\News
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\News")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NEWS_ID", referencedColumnName="NEWS_ID")
     * })
     */
    private $news;



    /**
     * Get newsFileId.
     *
     * @return string
     */
    public function getNewsFileId()
    {
        return $this->newsFileId;
    }

    /**
     * Set file.
     *
     * @param \App\Entities\FileMaster|null $file
     *
     * @return NewsFile
     */
    public function setFile(\App\Entities\FileMaster $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \App\Entities\FileMaster|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set news.
     *
     * @param \App\Entities\News|null $news
     *
     * @return NewsFile
     */
    public function setNews(\App\Entities\News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news.
     *
     * @return \App\Entities\News|null
     */
    public function getNews()
    {
        return $this->news;
    }
}
