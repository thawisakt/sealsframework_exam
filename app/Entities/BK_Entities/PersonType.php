<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonType
 *
 * @ORM\Table(name="PERSON_TYPE")
 * @ORM\Entity
 */
class PersonType
{
    /**
     * @var int
     *
     * @ORM\Column(name="PERSON_TYPE_CD", type="integer", nullable=false, options={"comment"="รหัสประเภทบุคลากร"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="PERSON_TYPE_PERSON_TYPE_CD_seq", allocationSize=1, initialValue=1)
     */
    private $personTypeCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_TYPE_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อประเภทบุคลากร"})
     */
    private $personTypeName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="PERSON_TYPE_KEY", type="string", length=1, nullable=true, options={"comment"="รหัสที่ใช้ในการ Generate Person ID"})
     */
    private $personTypeKey;



    /**
     * Get personTypeCd.
     *
     * @return int
     */
    public function getPersonTypeCd()
    {
        return $this->personTypeCd;
    }

    /**
     * Set personTypeName.
     *
     * @param string|null $personTypeName
     *
     * @return PersonType
     */
    public function setPersonTypeName($personTypeName = null)
    {
        $this->personTypeName = $personTypeName;

        return $this;
    }

    /**
     * Get personTypeName.
     *
     * @return string|null
     */
    public function getPersonTypeName()
    {
        return $this->personTypeName;
    }

    /**
     * Set personTypeKey.
     *
     * @param string|null $personTypeKey
     *
     * @return PersonType
     */
    public function setPersonTypeKey($personTypeKey = null)
    {
        $this->personTypeKey = $personTypeKey;

        return $this;
    }

    /**
     * Get personTypeKey.
     *
     * @return string|null
     */
    public function getPersonTypeKey()
    {
        return $this->personTypeKey;
    }
}
