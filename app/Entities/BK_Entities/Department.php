<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * Department
 *
 * @ORM\Table(name="DEPARTMENT")
 * @ORM\Entity
 */
class Department
{
    /**
     * @var int
     *
     * @ORM\Column(name="DEPT_CD", type="integer", nullable=false, options={"comment"="รหัสหน่วยงาน"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="DEPARTMENT_DEPT_CD_seq", allocationSize=1, initialValue=1)
     */
    private $deptCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="DEPT_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อหน่วยงาน"})
     */
    private $deptName;


    /**
     * Set deptCd.
     *
     * @param int|null $deptCd
     *
     * @return Department
     */
    public function setDeptCd($deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return int
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set deptName.
     *
     * @param string|null $deptName
     *
     * @return Department
     */
    public function setDeptName($deptName = null)
    {
        $this->deptName = $deptName;

        return $this;
    }

    /**
     * Get deptName.
     *
     * @return string|null
     */
    public function getDeptName()
    {
        return $this->deptName;
    }
}
