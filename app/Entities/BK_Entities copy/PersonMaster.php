<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonMaster
 *
 * @ORM\Table(name="PERSON_MASTER", indexes={@ORM\Index(name="IDX_585DA819C3C4B237", columns={"DEPT_CD"}), @ORM\Index(name="IDX_585DA81994C3C196", columns={"FILE_ID"}), @ORM\Index(name="IDX_585DA819372E891B", columns={"PERSON_TYPE_CD"}), @ORM\Index(name="IDX_585DA819922512C4", columns={"POSITION_CD"})})
 * @ORM\Entity
 */
class PersonMaster
{
    /**
     * @var string
     *
     * @ORM\Column(name="PERSON_ID", type="string", length=6, nullable=false, options={"comment"="รหัสบุคลากร"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="PERSON_MASTER_PERSON_ID_seq", allocationSize=1, initialValue=1)
     */
    private $personId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FIRST_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อ"})
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="LAST_NAME", type="string", length=100, nullable=true, options={"comment"="นามสกุล"})
     */
    private $lastName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="BIRTH_DATE", type="date", nullable=true, options={"comment"="วันเกิด"})
     */
    private $birthDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="ID_CARD", type="string", length=13, nullable=true, options={"comment"="เลขที่บัตรประชาชน"})
     */
    private $idCard;

    /**
     * @var \App\Entities\Department
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DEPT_CD", referencedColumnName="DEPT_CD")
     * })
     */
    private $deptCd;

    /**
     * @var \App\Entities\FileMaster
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\FileMaster")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FILE_ID", referencedColumnName="FILE_ID")
     * })
     */
    private $file;

    /**
     * @var \App\Entities\PersonType
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\PersonType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSON_TYPE_CD", referencedColumnName="PERSON_TYPE_CD")
     * })
     */
    private $personTypeCd;

    /**
     * @var \App\Entities\Position
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Position")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="POSITION_CD", referencedColumnName="POSITION_CD")
     * })
     */
    private $positionCd;



    /**
     * Get personId.
     *
     * @return string
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return PersonMaster
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return PersonMaster
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birthDate.
     *
     * @param \DateTime|null $birthDate
     *
     * @return PersonMaster
     */
    public function setBirthDate($birthDate = null)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate.
     *
     * @return \DateTime|null
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set idCard.
     *
     * @param string|null $idCard
     *
     * @return PersonMaster
     */
    public function setIdCard($idCard = null)
    {
        $this->idCard = $idCard;

        return $this;
    }

    /**
     * Get idCard.
     *
     * @return string|null
     */
    public function getIdCard()
    {
        return $this->idCard;
    }

    /**
     * Set deptCd.
     *
     * @param \App\Entities\Department|null $deptCd
     *
     * @return PersonMaster
     */
    public function setDeptCd(\App\Entities\Department $deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return \App\Entities\Department|null
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set file.
     *
     * @param \App\Entities\FileMaster|null $file
     *
     * @return PersonMaster
     */
    public function setFile(\App\Entities\FileMaster $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return \App\Entities\FileMaster|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set personTypeCd.
     *
     * @param \App\Entities\PersonType|null $personTypeCd
     *
     * @return PersonMaster
     */
    public function setPersonTypeCd(\App\Entities\PersonType $personTypeCd = null)
    {
        $this->personTypeCd = $personTypeCd;

        return $this;
    }

    /**
     * Get personTypeCd.
     *
     * @return \App\Entities\PersonType|null
     */
    public function getPersonTypeCd()
    {
        return $this->personTypeCd;
    }

    /**
     * Set positionCd.
     *
     * @param \App\Entities\Position|null $positionCd
     *
     * @return PersonMaster
     */
    public function setPositionCd(\App\Entities\Position $positionCd = null)
    {
        $this->positionCd = $positionCd;

        return $this;
    }

    /**
     * Get positionCd.
     *
     * @return \App\Entities\Position|null
     */
    public function getPositionCd()
    {
        return $this->positionCd;
    }
}
