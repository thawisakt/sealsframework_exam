<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Table(name="NEWS", indexes={@ORM\Index(name="IDX_2B4674E4C3C4B237", columns={"DEPT_CD"}), @ORM\Index(name="IDX_2B4674E43C4E1559", columns={"NEWS_TYPE_CD"})})
 * @ORM\Entity
 */
class News
{
    /**
     * @var string
     *
     * @ORM\Column(name="NEWS_ID", type="string", length=8, nullable=false, options={"comment"="รหัสข่าว"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="NEWS_NEWS_ID_seq", allocationSize=1, initialValue=1)
     */
    private $newsId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="BUD_YEAR", type="integer", nullable=true, options={"comment"="ปีงบประมาณ"})
     */
    private $budYear;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="START_DATE", type="date", nullable=true, options={"comment"="วันที่เริ่มต้นแจ้งข่าว"})
     */
    private $startDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="END_DATE", type="date", nullable=true, options={"comment"="วันที่สิ้นสุดแจ้งข่าว"})
     */
    private $endDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NEWS_TITLE", type="string", length=250, nullable=true, options={"comment"="หัวข้อข่าว"})
     */
    private $newsTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NEWS_DETAIL", type="string", length=4000, nullable=true, options={"comment"="รายละเอียดข่าว"})
     */
    private $newsDetail;

    /**
     * @var \App\Entities\Department
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\Department")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DEPT_CD", referencedColumnName="DEPT_CD")
     * })
     */
    private $deptCd;

    /**
     * @var \App\Entities\NewsType
     *
     * @ORM\ManyToOne(targetEntity="App\Entities\NewsType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NEWS_TYPE_CD", referencedColumnName="NEWS_TYPE_CD")
     * })
     */
    private $newsTypeCd;



    /**
     * Get newsId.
     *
     * @return string
     */
    public function getNewsId()
    {
        return $this->newsId;
    }

    /**
     * Set budYear.
     *
     * @param int|null $budYear
     *
     * @return News
     */
    public function setBudYear($budYear = null)
    {
        $this->budYear = $budYear;

        return $this;
    }

    /**
     * Get budYear.
     *
     * @return int|null
     */
    public function getBudYear()
    {
        return $this->budYear;
    }

    /**
     * Set startDate.
     *
     * @param \DateTime|null $startDate
     *
     * @return News
     */
    public function setStartDate($startDate = null)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate.
     *
     * @return \DateTime|null
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate.
     *
     * @param \DateTime|null $endDate
     *
     * @return News
     */
    public function setEndDate($endDate = null)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate.
     *
     * @return \DateTime|null
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set newsTitle.
     *
     * @param string|null $newsTitle
     *
     * @return News
     */
    public function setNewsTitle($newsTitle = null)
    {
        $this->newsTitle = $newsTitle;

        return $this;
    }

    /**
     * Get newsTitle.
     *
     * @return string|null
     */
    public function getNewsTitle()
    {
        return $this->newsTitle;
    }

    /**
     * Set newsDetail.
     *
     * @param string|null $newsDetail
     *
     * @return News
     */
    public function setNewsDetail($newsDetail = null)
    {
        $this->newsDetail = $newsDetail;

        return $this;
    }

    /**
     * Get newsDetail.
     *
     * @return string|null
     */
    public function getNewsDetail()
    {
        return $this->newsDetail;
    }

    /**
     * Set deptCd.
     *
     * @param \App\Entities\Department|null $deptCd
     *
     * @return News
     */
    public function setDeptCd(\App\Entities\Department $deptCd = null)
    {
        $this->deptCd = $deptCd;

        return $this;
    }

    /**
     * Get deptCd.
     *
     * @return \App\Entities\Department|null
     */
    public function getDeptCd()
    {
        return $this->deptCd;
    }

    /**
     * Set newsTypeCd.
     *
     * @param \App\Entities\NewsType|null $newsTypeCd
     *
     * @return News
     */
    public function setNewsTypeCd(\App\Entities\NewsType $newsTypeCd = null)
    {
        $this->newsTypeCd = $newsTypeCd;

        return $this;
    }

    /**
     * Get newsTypeCd.
     *
     * @return \App\Entities\NewsType|null
     */
    public function getNewsTypeCd()
    {
        return $this->newsTypeCd;
    }
}
