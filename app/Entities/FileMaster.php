<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileMaster
 *
 * @ORM\Table(name="FILE_MASTER")
 * @ORM\Entity
 */
class FileMaster
{
    /**
     * @var int
     *
     * @ORM\Column(name="FILE_ID", type="integer", nullable=false, options={"comment"="รหัสไฟล์"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="FILE_MASTER_FILE_ID_seq", allocationSize=1, initialValue=1)
     */
    private $fileId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อไฟล์"})
     */
    private $fileName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_PATH", type="string", length=100, nullable=true, options={"comment"="Path ในการเก็บไฟล์"})
     */
    private $filePath;

    /**
     * @var string|null
     *
     * @ORM\Column(name="FILE_MIME_TYPE", type="string", length=100, nullable=true, options={"comment"="Mime Type"})
     */
    private $fileMimeType;


       /**
     * Set fileId.
     *
     * @param int|null $fileId
     *
     * @return FileMaster
     */
    public function setFileId($fileId = null)
    {
        $this->fileId = $fileId;

        return $this;
    }
    /**
     * Get fileId.
     *
     * @return int
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * Set fileName.
     *
     * @param string|null $fileName
     *
     * @return FileMaster
     */
    public function setFileName($fileName = null)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName.
     *
     * @return string|null
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set filePath.
     *
     * @param string|null $filePath
     *
     * @return FileMaster
     */
    public function setFilePath($filePath = null)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath.
     *
     * @return string|null
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set fileMimeType.
     *
     * @param string|null $fileMimeType
     *
     * @return FileMaster
     */
    public function setFileMimeType($fileMimeType = null)
    {
        $this->fileMimeType = $fileMimeType;

        return $this;
    }

    /**
     * Get fileMimeType.
     *
     * @return string|null
     */
    public function getFileMimeType()
    {
        return $this->fileMimeType;
    }
}
