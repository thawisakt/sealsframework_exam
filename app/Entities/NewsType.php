<?php

namespace App\Entities;

use Doctrine\ORM\Mapping as ORM;

/**
 * NewsType
 *
 * @ORM\Table(name="NEWS_TYPE")
 * @ORM\Entity
 */
class NewsType extends BaseEntity
{
    public function __construct(){
        $this->setPK('newsTypeCd');
    }
    /**
     * @var int
     *
     * @ORM\Column(name="NEWS_TYPE_CD", type="integer", nullable=false, options={"comment"="รหัสประเภทข่าว"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="NEWS_TYPE_NEWS_TYPE_CD_seq", allocationSize=1, initialValue=1)
     */
    private $newsTypeCd;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NEWS_TYPE_NAME", type="string", length=100, nullable=true, options={"comment"="ชื่อประเภทข่าว"})
     */
    private $newsTypeName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="NEWS_TYPE_KEY", type="string", length=1, nullable=true, options={"comment"="รหัสที่ใช้ในการ Generate news_id"})
     */
    private $newsTypeKey;


    /**
     * Set newsTypeCd.
     *
     * @param string|null $newsTypeCd
     *
     * @return NewsType
     */
    public function setNewsTypeCd($newsTypeCd = null)
    {
        $this->newsTypeCd = $newsTypeCd;

        return $this;
    }

    /**
     * Get newsTypeCd.
     *
     * @return int
     */
    public function getNewsTypeCd()
    {
        return $this->newsTypeCd;
    }

    /**
     * Set newsTypeName.
     *
     * @param string|null $newsTypeName
     *
     * @return NewsType
     */
    public function setNewsTypeName($newsTypeName = null)
    {
        $this->newsTypeName = $newsTypeName;

        return $this;
    }

    /**
     * Get newsTypeName.
     *
     * @return string|null
     */
    public function getNewsTypeName()
    {
        return $this->newsTypeName;
    }

    /**
     * Set newsTypeKey.
     *
     * @param string|null $newsTypeKey
     *
     * @return NewsType
     */
    public function setNewsTypeKey($newsTypeKey = null)
    {
        $this->newsTypeKey = $newsTypeKey;

        return $this;
    }

    /**
     * Get newsTypeKey.
     *
     * @return string|null
     */
    public function getNewsTypeKey()
    {
        return $this->newsTypeKey;
    }
}
