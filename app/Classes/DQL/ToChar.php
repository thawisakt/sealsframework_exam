<?php
namespace App\Classes\DQL;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
class ToChar extends FunctionNode
{
    private $arithmeticExpression;
    private $secondDateExpression;
	public function getSql(SqlWalker $sqlWalker)
	{
        //echo "11</br>";
        //var_dump($sqlWalker) ;
        return 'TO_CHAR(' . 
        $sqlWalker->walkSimpleArithmeticExpression($this->arithmeticExpression). ','.  //เนื่องจาก , เป็น string
        $sqlWalker->walkSimpleArithmeticExpression($this->secondDateExpression)
        .  ')';
	}
	public function parse(\Doctrine\ORM\Query\Parser $parser)
	{

        $lexer = $parser->getLexer();
        //var_dump($lexer);

		$parser->match(Lexer::T_IDENTIFIER);
		$parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->arithmeticExpression = $parser->SimpleArithmeticExpression();
        $parser->match(Lexer::T_COMMA);  //การมี 2 parameter ต้องใช้ T_COMMA 
        $this->secondDateExpression = $parser->SimpleArithmeticExpression();
		$parser->match(Lexer::T_CLOSE_PARENTHESIS);
	}
}
