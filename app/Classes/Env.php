<?php
namespace App\Classes;
class Env{
    public function __construct($p_base_path){
        $dotenv = \Dotenv\Dotenv::create($p_base_path);
        $dotenv->load();
    }
}