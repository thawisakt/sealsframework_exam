<?php

namespace App\Classes;
use App\Entities\SysGenerate;
//use App\Entities\Title;

use Doctrine\ORM\Id\AbstractIdGenerator;

class LastGenerator extends AbstractIdGenerator
{

    public function generate(\Doctrine\ORM\EntityManager $em, $entity)
    {
        $keycd = $entity->getKeyCd();
        $length = $entity->getLength();
        $entityName = get_class($entity);
       // \var_dump($entity);
       $tableName = $em->getClassMetadata($entityName)->getTableName();
        //\var_dump($str_entityName);
        //get ค่า last_no ล่าสุดจาก table last_number
        $result = $em->getRepository(SysGenerate::class)->findOneBy([
                                                                    'tableCd'=>$tableName,
                                                                    'keyCd'=>$keycd
                                                                ]);
        $last_no = 1;
        //check ว่ามีข้อมูลใน table last_number
        if($result!=null){
            //มีแล้วก็เพิ่มค่าและ update table last_number
            $last_no = $result->getLastNo();
            $last_no++;
            $result->setLastNo($last_no);
            $em->flush();
        }else{
            //ยังไม่มี ก็ให้เพิ่ม record ใน table last_number
            $result = new SysGenerate();
            $result->setTableCd($tableName);
            $result->setLastNo($last_no);
            $result->setKeyCd($keycd);
            $em->persist($result);
            $em->flush();
        }

        //ได้เลขสุดท้าย และ check ว่าต้อง generate พร้อม prefix หรือไม่
        //ถ้า prefix มากกว่า 0 (strlen คือ นับจำนวนตัวอักษรใน ตัวแปร $prefix)
        if(strlen($keycd)>0){
            //$str = "Hello World";
            $last_no = str_pad($last_no,$length,"0",STR_PAD_LEFT);
            return $keycd .$last_no;
        }else{
            return $last_no;
        }
     


        //return 200;
    }
}
