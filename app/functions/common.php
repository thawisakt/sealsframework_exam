<?php
    use eftec\bladeone;
    //use eftec\bladeone\BladeOneHtml;
    use eftec\bladeone\BladeOneHtmlBootstrap;
    class myBlade extends bladeone\BladeOne
    {
        use BladeOneHtmlBootstrap;
        protected function compileMyFunction($name){
            return $this->phpTag."echo \"<input type='text' value=".$name.">\";?>";
        }
    }

    function view($path, array $data = [])
    {
        $view = __DIR__ . '/../../resources/views'; //  /../ ถอย 1 folder
       // print($view);
        $cache = __DIR__ . '/../../bootstrap/cache';
            //$blade = new bladeone\BladeOne($view,$cache,0); //bladeone เป็น templat engine  0 คือ 
            $blade = new myBlade($view,$cache);
            $blade->setBaseUrl(getenv('APP_URL'));
            $blade->addAssetDict('js/jquery','https://code.jquery.com/jquery-3.3.1.min.js');


        echo $blade->run($path,$data);
    }
    function route($url)
    {
        $strAppPath = getenv('APP_PATH');   //phpdotEnv เป็นการสั่งให้อ่านไฟล์ .env
        $strLastChr = substr($strAppPath,strlen($strAppPath),1); //get last char
        if ($strLastChr != "/") { //  check ว่า APP_PATH ตัวสุดท้ายใส่ / 
            $strAppPath .= "/";
        }
        return $strAppPath . $url;
    }