<?php
namespace App\Controllers;
use App\Repositories\NewstypeRepository;
use App\Repositories\DepartmentRepository;
use App\Repositories\NewsRepository;
use App\Repositories\FilemasterRepository;
use App\Entities\FileMaster;
use App\Entities\News;
use App\Entities\SysGenerate;
use App\Entities\NewsFile;
use App\Entities\Department;
use App\Entities\NewsType;

class NewsController{
    public function index(){
        view('news');
    }
    public function listData(){
        $length = $_POST["length"];
        $start = $_POST["start"];   
        $intOrder = $_POST["order"][0]["column"];
        $orderColName = $_POST["columns"][$intOrder]["name"];
        $strOrderDirection = $_POST["order"][0]["dir"]; 
		$rep = new NewsRepository();
		$result_paginator = $rep->listByPage($start,$length,$orderColName,$strOrderDirection);      
        $total = $result_paginator->count();
        //var_dump($total);
        $data = array();
        foreach ($result_paginator as $row) {
            $data[] = array(
                        'newsId'=>$row['newsId'],
                        'deptName' =>$row['deptName'],
                        'newsTitle'=>$row['newsTitle'],
                        'startDate'=>$row['startDate'],
                        'endDate'=>$row['endDate'],
            );
            //var_dump($data);
        }
            $alldata=array(
                'data'=>$data,
                'recordTotal'=>$total,
                'recordsFiltered'=>$total,
               // 'order'=>$intOrder,
               // 'dir'=>$strOrderDirection,
                //"draw"=>$_POST["draw"]
            );
    
       echo json_encode($alldata);
    }
    public function loadNewstype(){
        $rep = new NewstypeRepository();
        $rsNewstype = $rep->list();
        foreach ($rsNewstype as $value) {
            $data[] = array(
                'NewTypeid'=> $value['newsTypeCd'],
                'NewsTypename'=> $value['newsTypeName']
            );
        }
        echo json_encode($data);
    }

    public function upload(){
       // \var_dump($_REQUEST['param']);
        $filenameNews =array();
        $newsFile = array();
        $uploaddir = getenv('FILES');
        $tmp_name = $_FILES["file"]["tmp_name"];
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        //\var_dump($finfo);
        $count = count($tmp_name);
        for ($i=0; $i < $count; $i++) { 
            $filename = $_REQUEST['param'][$i];
            //\var_dump($filename);
           // $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime_type = finfo_file($finfo, $tmp_name[$i]);

            mt_srand((double)microtime()*10000);
            $charid = strtoupper(md5(uniqid(rand(), true)));
            $hyphen =chr(45);
            $uuid = 
                substr($charid,0,8).$hyphen
                .substr($charid, 8, 4).$hyphen
                .substr($charid, 12, 4).$hyphen
                .substr($charid,16, 4).$hyphen
                .substr($charid, 20, 12);
            //\var_dump($mime_type);

            //\var_dump($_FILES['file']['name'][$i]);
            
            
            $type = end(explode('/', $mime_type));
            //\var_dump($mime_type);
            $uploadfiles = $uploaddir."/".$uuid.".".$type;
            move_uploaded_file($tmp_name[$i],$uploadfiles);
            
            $filepath = substr($uploaddir,28);
            $repFilemaster = new FilemasterRepository();
            $filemaster = new FileMaster();
            $filemaster->setFileName($filename);
            $filemaster->setFilePath($filepath);
            $filemaster->setFileMimeType($mime_type);
            //$filenameNews[] = $filemaster;
            //$news_file = new NewsFile();
            //$newsFile[] = $news_file;
            $repFilemaster->save($filemaster);

        }
    }

    public function save(){
      //  \var_dump($_REQUEST['frm']);
     //   \var_dump($_REQUEST['files']);

        if(!empty($_REQUEST['txtBudyear'])){
            $budyear = $_REQUEST['txtBudyear'];
            $str_budyear = substr($budyear,2);
        }else{
            $budyear = null;
        }
        if(!empty($_REQUEST['slNewsType'])){
            $newstype = $_REQUEST['slNewsType'];
        }else{
            $newstype = null;
        }

        $repNewsType = new NewstypeRepository();
        $rsNewstype = $repNewsType->get($newstype);
        //\var_dump($rsNewstype['newsTypeKey']);
        $newtypekey = $rsNewstype['newsTypeKey'];


        if(!empty($_REQUEST['txtDeptId'])){
            $dept = $_REQUEST['txtDeptId'];
        }else{
            $dept = null;
        }
        if(!empty($_REQUEST['txtNewsTitle'])){
            $newstitle = $_REQUEST['txtNewsTitle'];
        }else{
            $newstitle = null;
        }
        if(!empty($_REQUEST['txtStartDate'])){
            $startdate = $_REQUEST['txtStartDate'];
            $str_startdate = str_replace('/','-',$startdate);
            $new_startdate = date('Y-m-d 00:00:00',strtotime($str_startdate));
            $create_startdate = date_create($new_startdate);
        }else{
            $create_startdate = null;
        }
        if(!empty($_REQUEST['txtEndDate'])){
            $enddate = $_REQUEST['txtEndDate'];
            $str_enddate = str_replace('/','-',$enddate);
            $new_enddate = date('Y-m-d 00:00:00',strtotime($str_enddate));
            $create_enddate = date_create($new_enddate);
        }else{
            $create_enddate = null;
        }
        if(!empty($_REQUEST['summernote'])){
            $newsdetail = $_REQUEST['summernote'];
        }else{
            $newsdetail = null;
        }
        // $filenameNews =array();
        // $newsFile = array();
        // if(!empty($_REQUEST['txtfilename'])){
        //     for ($i=0; $i < count($_REQUEST['txtfilename']); $i++) { 
        //         $filename = $_REQUEST['txtfilename'][$i];
        //         $tmp_name = $_FILES["file"]["tmp_name"][$i];
        //         //\var_dump($tmp_name);
        //         $finfo = finfo_open(FILEINFO_MIME_TYPE);
        //         $mime_type = finfo_file($finfo, $tmp_name);
        //         $uploaddir = getenv('FILES');
        //         $uploadfiles = $uploaddir."/".basename($_FILES['file']['name'][$i]);
        //         move_uploaded_file($tmp_name,$uploadfiles);
                
        //         $filepath = substr($uploaddir,28);
        //         $filemaster = new FileMaster();
        //         $filemaster->setFileName($filename);
        //         $filemaster->setFilePath($filepath);
        //         $filemaster->setFileMimeType($mime_type);
        //         $filenameNews[] = $filemaster;
        //         $news_file = new NewsFile();
        //         $newsFile[] = $news_file;

        //     }
        // }
        if(!empty($_REQUEST['hidtxtNewsId'])){
            $repNews = new NewsRepository();
            $news = new News();
            $news->setNewsId($_REQUEST['hidtxtNewsId']);
            $repNT = new NewstypeRepository();
            $news->setNewsTypeCd($repNT->getid($newstype));
            $repdept = new DepartmentRepository();
            $news->setDeptCd($repdept->getid($dept));
            $news->setBudYear($budyear);
            $news->setStartDate($create_startdate);
            $news->setEndDate($create_enddate);
            $news->setNewsTitle($newstitle);
            $news->setNewsDetail($newsdetail);
            $repNews->save($news);

            echo \json_encode(array('ms_alert'=>'Edit success'));
        }else{
            $repNews = new NewsRepository();
            $news = new News();
            $keycd = $str_budyear.$newtypekey;
            $news->setRunning($keycd,5);
            $repNT = new NewstypeRepository();
            $news->setNewsTypeCd($repNT->getid($newstype));
            $repdept = new DepartmentRepository();
            $news->setDeptCd($repdept->getid($dept));
            $news->setBudYear($budyear);
            $news->setStartDate($create_startdate);
            $news->setEndDate($create_enddate);
            $news->setNewsTitle($newstitle);
            $news->setNewsDetail($newsdetail);
            $repNews->save($news);
            echo \json_encode(array('ms_alert'=>'Save success'));
        }
       

      }
      
      public function getdata($param){
          //\var_dump($param);
          $rep = new NewsRepository();
          $rsNews = $rep->getdata($param['a']);
          //\var_dump($rsNews['newsId']);
              $data[] = array(
                  'newsid'=> $rsNews['newsId'],
                  'newstypecd'=> $rsNews['newsTypeCd'],
                  'newstypename'=> $rsNews['newsTypeName'],
                  'deptcd'=> $rsNews['deptCd'],
                  'deptname'=> $rsNews['deptName'],
                  'budyear'=> $rsNews['budYear'],
                  'startdate'=> $rsNews['startDate'],
                  'enddate'=> $rsNews['endDate'],
                  'newstitle'=> $rsNews['newsTitle'],
                  'newsdetail'=> $rsNews['newsDetail'],
              );

         echo json_encode($data);
      }

}