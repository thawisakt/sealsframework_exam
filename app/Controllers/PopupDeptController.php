<?php
namespace App\Controllers;
use App\Repositories\DepartmentRepository;

class  PopupDeptController
{
    public function loadData(){
        if(!empty($_POST['deptid'])){
            $deptid = $_POST['deptid'];
        }else{
            $deptid= null;
        }
        if(!empty($_POST['deptname'])){
            $deptname = $_POST['deptname'];
        }else{
            $deptname = null;
        }
        
        $start = $_POST['start'];
        $length = $_POST['length'];
        $intOrder = $_POST["order"][0]["column"];
        $orderColName = $_POST["columns"][$intOrder]["data"];
        $strOrderDirection = $_POST["order"][0]["dir"]; 
        $rep = new DepartmentRepository();
        $result_paginator = $rep->listByNamePage($deptid, $deptname,$start,$length,$orderColName,$strOrderDirection);
        $total = $result_paginator->count();
         $data = array();
        foreach ($result_paginator as $row) {
            $data[] = array(
                'deptCd' => $row['deptCd'],
                'deptName' => $row['deptName']
            );
        }
        $alldata = array(
            'data' => $data,
            'recordTotal'=>$total,
            'recordsFiltered'=>$total
        );
        echo json_encode($alldata);
    }
    // public function loadData(){
    //     if(!empty($_POST['deptid'])){
    //         $deptid = $_POST['deptid'];
    //     }else{
    //         $deptid= null;
    //     }
    //     if(!empty($_POST['deptname'])){
    //         $deptname = $_POST['deptname'];
    //     }else{
    //         $deptname = null;
    //     }
    //     $rep = new DepartmentRepository();
    //         $result_paginator = $rep->list();
    //         $data = array();
    //         foreach ($result_paginator as $row) {
    //             $data[] = array(
    //                 'deptid' => $row['deptCd'],
    //                 'deptname' => $row['deptName']
    //             );
    //         }
    //         // $alldata = array(
    //         //     'data' => $data,
    //         //     'recordTotal'=>$total,
    //         //     'recordsFiltered'=>$total
    //         // );
    //         echo json_encode($data);
    // }
}
