<?php

if(!isset($_SESSION)) session_start();

define('BASE_PATH', realpath(__DIR__.'/../'));
//print(BASE_PATH);

date_default_timezone_set("Asia/Bangkok");
$loader = require_once BASE_PATH . '/vendor/autoload.php';

/**
 *  PHP DotENV
 */
new \App\Classes\Env(BASE_PATH);

/**
 * Routing
 */
$router = new AltoRouter();
$router->setBasePath(getenv('APP_PATH'));

require_once BASE_PATH.'/app/routing/web.php';
new \App\Classes\RoutingDispatcher($router);