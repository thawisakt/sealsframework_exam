var gulp = require('gulp');
var minifyJS = require('gulp-uglify');
var rename = require('gulp-rename');
var minifyCSS = require('gulp-minify-css');

var jsFile = [
    './resources/assets/libs/jquery/dist/jquery.min.js',
    './resources/assets/libs/bootstrap/dist/js/bootstrap.min.js',
    './resources/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js',
    './resources/assets/libs/select2/dist/js/select2.min.js',
    './resources/assets/libs/dropzone/dist/min/dropzone.min.js',
    './resources/assets/libs/summernote/dist/summernote-bs4.min.js',
    './resources/assets/libs/moment/min/moment.min.js',
    './resources/assets/libs/datatables.net/js/jquery.dataTables.min.js',
    './resources/assets/js/dataTables.bootstrap4.min.js',
    './node_modules/flatpickr/dist/flatpickr.min.js',
    './resources/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js',
    './resources/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js',
    './resources/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js',
    './resources/assets/libs/jszip/dist/jszip.min.js',
    './resources/assets/libs/pdfmake/build/pdfmake.min.js',
    './resources/assets/libs/datatables.net-buttons/js/buttons.flash.min.js',
    './resources/assets/libs/datatables.net-buttons/js/buttons.html5.min.js',
];

var cssFile = [
    './resources/assets/libs/bootstrap/dist/css/bootstrap.min.css',
    './resources/assets/libs/dropzone/dist/min/dropzone.min.css',
    './resources/assets/libs/select2/dist/css/select2.min.css',
    './resources/assets/css/summernote/summernote-bs4.css',
    './resources/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
    './node_modules/flatpickr/dist/flatpickr.min.css',
    './resources/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css',
    './resources/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css'
];

var fontFile = [
    './resources/assets/libs/summernote/dist/font/summernote.eot',
    './resources/assets/libs/summernote/dist/font/summernote.ttf',
    './resources/assets/libs/summernote/dist/font/summernote.woff'
];

gulp.task('libfonts',function(){
    return gulp.src(fontFile)
    .pipe(gulp.dest('./wwwroot/fonts'));
});

gulp.task('libjs',function(){
    return gulp
    .src(jsFile)
    .pipe(gulp.dest('./wwwroot/js'));
});

gulp.task('libcss',function(){
    return gulp
    .src(cssFile)
    .pipe(gulp.dest('./wwwroot/css'));
});

//-------------------convert datatables js to min-------------------//
gulp.task('minDatatables',function(){
    return gulp
    .src('./resources/assets/js/dataTables.bootstrap4.js')
    .pipe(minifyJS())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest('./resources/assets/js'));
});
/*------------------convert common.js to .min and copy to wwroot------------------------*/
gulp.task('minCommon',function(){
    return gulp
    .src('./resources/assets/js/common.js')
    .pipe(minifyJS())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest('./resources/assets/js'));
});

gulp.task('jscommon',function(){
    return gulp
    .src('./resources/assets/js/common.min.js')
    .pipe(gulp.dest('./wwwroot/js'));
});

/*-----------------convert dataTable.css to .min.css and copy to wwroot---------------*/
gulp.task('minDatatableCSS',function(){
    return gulp
    .src('./resources/assets/css/datatables/dataTables.bootstrap4.css')
    .pipe(minifyCSS())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest('./resources/assets/css/datatables'));
});

gulp.task('cssDatatables', function(){
    return gulp
    .src('./resources/assets/css/datatables/dataTables.bootstrap4.min.css')
    .pipe(gulp.dest('./wwwroot/css'));
});

/*-----------convert vsf_fonts.js to .min and copy to wwwroot------------- */
gulp.task('minvsf_fonts',function(){
    return gulp
    .src('./resources/assets/js/vfs_fonts.js')
    .pipe(minifyJS())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest('./resources/assets/js'));
});
gulp.task('libvsf_fonts',function(){
    return gulp
    .src('./resources/assets/js/vfs_fonts.min.js')
    .pipe(gulp.dest('./wwwroot/js'));
});
/*-----------convert pdfmake.js to .min and copy to wwwroot------------- */
gulp.task('minpdf_make',function(){
    return gulp
    .src('./resources/assets/js/pdfmake.js')
    .pipe(minifyJS())
    .pipe(rename({suffix:'.min'}))
    .pipe(gulp.dest('./resources/assets/js'));
});
gulp.task('libpdf_make',function(){
    return gulp
    .src('./resources/assets/js/pdfmake.min.js')
    .pipe(gulp.dest('./wwwroot/js'));
});
gulp.task('all',gulp.series(['libjs','libcss']));