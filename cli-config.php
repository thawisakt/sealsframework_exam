<?php
require_once('vendor/autoload.php');
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

$isDevMode = false;
$connectionParams = array( 
 'url'=> 'oci8://sealsdev:seals@localhost/orcl?charset=utf8' //กำหนด url ของการเชื่อมต่อ Oracle
);
$paths = array(__DIR__ . "/app/Entities");
$config = Setup::createAnnotationMetadataConfiguration($paths,$isDevMode, null, null, false);
$entityManager = EntityManager::create($connectionParams,$config);

// if($entityManager){
//     echo "connect";
// }else{
//     echo "not connect";
// }
//------------------------------------------------//

return ConsoleRunner::createHelperSet($entityManager);